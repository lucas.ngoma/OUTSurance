﻿using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace Assessment
{
    /// <summary>
    /// Represents the program class.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// The log.
        /// </summary>
        private static ILog log;

        /// <summary>
        /// Defines the entry point of the application.
        /// </summary>
        /// <param name="args">The arguments.</param>
        static void Main(string[] args)
        {
            log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

            IList<Person> people;

            try
            {
                log.Info("Assessment started.");

                var path = Path.Combine(Directory.GetCurrentDirectory(), "data.csv");

                log.Info("Assessment started data file found.");

                FileInput fi = new FileInput();

                people = fi.ProcessFile(path);

                log.Info("Assessment data file processed OK.");

                //Task 1.
                CompileTask1OutputToFile(people);                

                //Task 2.
                CompileTask2OutputToFile(people);

                //Generics Task 1.
                GenericsTask1OutputToFile(people);

            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                log.Error(ex.StackTrace);
            }
            finally
            {
                LogManager.Shutdown();
            }            
        }

        private static void GenericsTask1OutputToFile(IList<Person> people)
        {
            var sortedFrequency = people.SortFrequencyOrder();

            DateTime date = DateTime.Now.Date;

            var filePath = Path.Combine(Directory.GetCurrentDirectory(), $"{date.Month}_{date.Day}_{date.Year}_generics_task1.txt"); ;

            using (StreamWriter file = new StreamWriter(filePath, false))
            {
                foreach (var item in sortedFrequency)
                {
                    file.WriteLine($"{item.Key}, {item.Count}");
                }
            }

            log.Info("Assessment task1 completed successful.");
        }

        /// <summary>
        /// Compiles the task1 output to file on the bin directory.
        /// </summary>
        /// <param name="people">The people.</param>
        private static void CompileTask1OutputToFile(IList<Person> people)
        {
            var sortedFrequency = people.Task1SortOrderWithFrequency();

            DateTime date = DateTime.Now.Date;

            var filePath = Path.Combine(Directory.GetCurrentDirectory(), $"{date.Month}_{date.Day}_{date.Year}_task1.txt"); ;

            using (StreamWriter file = new StreamWriter(filePath, false))
            {
                foreach (var item in sortedFrequency)
                {
                    file.WriteLine($"{item.Key}, {item.Value}");
                }
            }

            log.Info("Assessment task1 completed successful.");

        }

        /// <summary>
        /// Compiles the task2 output to file on the bin directory.
        /// </summary>
        /// <param name="people">The people.</param>
        private static void CompileTask2OutputToFile(IList<Person> people)
        {
            var sortAddresses = people.Task2SortAddress();

            DateTime date = DateTime.Now.Date;
                        
            var filePath = Path.Combine(Directory.GetCurrentDirectory(), $"{date.Month}_{date.Day}_{date.Year}_task2.txt"); ;
            
            using (StreamWriter file = new StreamWriter(filePath, false))
            {
                foreach (var item in sortAddresses)
                {
                    file.WriteLine($"{item.Address.StreetNumber} {item.Address.StreetName}");
                }
            }

            log.Info("Assessment task2 completed successful.");
        }
    }
}
