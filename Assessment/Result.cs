﻿namespace Assessment
{
    public class Result
    {
        public string Key { get; set; }

        public int Count { get; set; }
    }

}
