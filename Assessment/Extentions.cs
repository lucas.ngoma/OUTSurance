﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Assessment
{
    /// <summary>
    /// Represents the extensions class.
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// Adds the or update.
        /// </summary>
        /// <param name="dictionary">The dictionary.</param>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        public static void AddOrUpdate<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key, TValue value)
        {
            if (dictionary.ContainsKey(key))
            {
                dictionary[key] = value;
            }
            else
            {
                dictionary.Add(key, value);
            }
        }

        /// <summary>
        /// Task2s the sort address.
        /// </summary>
        /// <param name="list">The list.</param>
        /// <returns>An IList&lt;Person&gt; list.</returns>
        public static IList<T> Task2SortAddress<T>(this IList<T> list)
            where T: Person
        {
            return list.OrderBy(p => p.Address.StreetName).ToList();
        }

        /// <summary>
        /// Task1s the sort order with frequency.
        /// </summary>
        /// <param name="listOfPersons">The list of persons.</param>
        /// <returns>A List&lt;KeyValuePair&lt;System.String, System.Int32&gt;&gt; list.</returns>
        public static List<KeyValuePair<string, int>> Task1SortOrderWithFrequency<T>(this IList<T> listOfPersons)
            where T: Person
        {
            var frequency = new Dictionary<string, int>();

            foreach (var person in listOfPersons)
            {
                AddKeyValue(frequency, person.Name);
                AddKeyValue(frequency, person.Surname);
            }
            
            return frequency.OrderByDescending(x => x.Value).ThenBy(x => x.Key).ToList();            
        }

        public static List<Result> SortFrequencyOrder<T>(this IList<T> list)
            where T : Person            
        {            
            var namegroup =
                 from person in list
                 group person by person.Name into groupPeople                 
                 select new Result { Key = groupPeople.Key, Count = groupPeople.Count() };

            var surnameGroup =
                from person in list
                group person by person.Surname into groupPeople
                select new Result { Key = groupPeople.Key, Count = groupPeople.Count() };

            var count = namegroup.Concat(surnameGroup);

            return count.OrderByDescending(x => x.Count).ThenBy(x => x.Key).ToList();
           
        }
        
        private static List<Result> SortOrderBy<T, TKey>(this IList<T> list, TKey orderByKey)
            where T: Person
        {
            var grouping =
               from person in list
               group person by orderByKey into groupPeople
               select new Result { Key = groupPeople.Key.ToString(), Count = groupPeople.Count() };

            return grouping.ToList();
        }
        /// <summary>
        /// Adds the key value.
        /// </summary>
        /// <param name="frequency">The frequency.</param>
        /// <param name="keyValue">The key value.</param>
        private static void AddKeyValue(Dictionary<string, int> frequency, string keyValue)
        {
            if (frequency.ContainsKey(keyValue))
            {
                int value;
                frequency.TryGetValue(keyValue, out value);

                value += 1;

                frequency.AddOrUpdate(keyValue, value);
            }
            else
            {
                frequency.AddOrUpdate(keyValue, 1);
            }
        }
    }

}
