﻿using System;

namespace Assessment
{
    /// <summary>
    /// Represents the address class.
    /// </summary>
    public class Address
    {
        /// <summary>
        /// Gets or sets the street number.
        /// </summary>
        /// <value>The street number.</value>
        public int StreetNumber
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the name of the street.
        /// </summary>
        /// <value>The name of the street.</value>
        public string StreetName
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the phone number.
        /// </summary>
        /// <value>The phone number.</value>
        public int PhoneNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Address"/> class.
        /// </summary>
        /// <param name="streetAddress">The street address.</param>
        public Address(string streetAddress)
        {
            SetAddressDetails(streetAddress);
        }

        /// <summary>
        /// Sets the address details.
        /// </summary>
        /// <param name="streetAddress">The street address.</param>
        private void SetAddressDetails(string streetAddress)
        {
            if (!string.IsNullOrWhiteSpace(streetAddress))
            {
                var values = streetAddress.Split(' ');

                if (values != null && values.Length > 2)
                {
                    StreetNumber = Convert.ToInt32(values[0]);
                    StreetName = $"{values[1]} {values[2]}";
                }
            }
        }
    }
}
