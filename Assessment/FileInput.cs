﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Assessment
{
    /// <summary>
    /// Represents the file input class.
    /// </summary>
    public class FileInput
    {
        /// <summary>
        /// Gets or sets the people.
        /// </summary>
        /// <value>The people.</value>
        public List<Person> people
        {
            get; set;
        }
        /// <summary>
        /// Processes the file.
        /// </summary>
        /// <param name="filepath">The filepath.</param>
        /// <returns>An IList&lt;Person&gt; list.</returns>
        public IList<Person> ProcessFile(string filepath)
        {           
            try
            {
                people = File.ReadAllLines(filepath)
                            .Skip(1)
                            .Select(v => Add(v)).ToList();                          

                return people;
            }
            catch (Exception ex)
            {
                throw ex;
            }           
        }

        /// <summary>
        /// Adds the specified line.
        /// </summary>
        /// <param name="line">The line.</param>
        /// <returns>A Person object instance.</returns>
        /// <exception cref="System.FormatException">
        /// An error occurred while processing the data file. The format of the data file does meet expected requirement
        /// or
        /// An error occurred while processing the data file. The format of the data file does meet expected requirement
        /// </exception>
        private Person Add(string line)
        {
            if (!string.IsNullOrWhiteSpace(line))
            {
                var values = line.Split(',');
                
                if (values != null && values.Length > 3)
                {
                    Person person = new Person
                    {
                        Name = values[0],
                        Surname = values[1],
                        Address = new Address(values[2])                        
                    };

                    person.Address.PhoneNumber = Convert.ToInt32(values[3]);

                    return person;
                }
                else
                {
                    throw new FormatException("An error occurred while processing the data file. The format of the data file does meet expected requirement");
                }                
            }
            else
            {
                throw new FormatException("An error occurred while processing the data file. The format of the data file does meet expected requirement");
            }            
        }
    }
}
