﻿using Assessment;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Assessment.Tests
{
    /// <summary>
    /// Represents the extensions tests class.
    /// </summary>
    [TestClass()]
    public class ExtensionsTests
    {
        /// <summary>
        /// Givens the key and value where key exists when add or update then add key and value.
        /// </summary>
        [TestMethod()]
        [TestCategory("Unit")]
        public void Given_KeyAndValueWhereKeyExists_When_AddOrUpdate_Then_AddKeyAndValue()
        {
            // Arrange
            var key = "key";
            var value = 1;
            var dictionary = new Dictionary<string, int>();

            // Act
            dictionary.AddOrUpdate(key, value);

            // Assert
            Assert.IsTrue(dictionary.ContainsKey(key));
            Assert.AreEqual(dictionary[key], value);
        }

        /// <summary>
        /// Given the key and value where key exists when add or update then add key and value.
        /// </summary>
        [TestMethod]
        [TestCategory("Unit")]
        public void Given_KeyAndValueWhereKeyNotExists_When_AddOrUpdate_Then_UpdateValue()
        {
            // Arrange
            var key = "key";
            var value = 1;
            var value2 = 2;
            var dictionary = new Dictionary<string, int>()
            {
                { key, value }
            };

            // Act
            dictionary.AddOrUpdate(key, value2);

            // Assert
            Assert.IsTrue(dictionary.ContainsKey(key));
            Assert.AreEqual(dictionary[key], value2);
        }

        /// <summary>
        /// Givens the key an uordered collection when task2 sort address then address are sorted.
        /// </summary>
        [TestMethod()]
        [TestCategory("Unit")]
        public void Given_KeyAnUorderedCollection_When_Task2SortAddress_Then_Address_Are_Sorted()
        {
            List<Person> people = new List<Person>();

            var z_p = new Person
            {
                Name = "Z_P",
                Surname = "Z_Surname",
                Address = new Address("123 Z_Street1 111")
            };

            var x_p = new Person
            {
                Name = "X_P",
                Surname = "X_Surname",
                Address = new Address("123 X_Street1 111")
            };

            var s_p = new Person
            {
                Name = "S_P1",
                Surname = "S_Surname1",
                Address = new Address("123 S_Street1 111")
            };

            var y_p = new Person
            {
                Name = "Y_P",
                Surname = "Y_Surname",
                Address = new Address("123 Y_Street1 1111")
            };

            //Act
            people.Add(z_p);
            people.Add(x_p);
            people.Add(s_p);
            people.Add(y_p);

            Assert.IsTrue(people.IndexOf(z_p) < people.IndexOf(x_p));
            Assert.IsTrue(people.IndexOf(x_p) < people.IndexOf(s_p));
            Assert.IsTrue(people.IndexOf(z_p) < people.IndexOf(s_p));

            //Asset
            var new_people = people.Task2SortAddress();
            Assert.IsTrue(new_people.IndexOf(z_p) > new_people.IndexOf(x_p));
            Assert.IsTrue(new_people.IndexOf(x_p) > new_people.IndexOf(s_p));
            Assert.IsTrue(new_people.IndexOf(z_p) > new_people.IndexOf(s_p));

        }

        /// <summary>
        /// Givens the key an uordered collection when task1 sort order with frequency test then order by desc then asc.
        /// </summary>
        [TestMethod()]
        public void Given_KeyAnUorderedCollection_When_Task1SortOrderWithFrequencyTest_Then_OrderBy_Desc_Then_Asc()
        {
            List<Person> people = new List<Person>();

            var z_p1 = new Person
            {
                Name = "Z_P",
                Surname = "Z_Surname",
                Address = new Address("123 Z_Street1 111")
            };

            var x_p = new Person
            {
                Name = "X_P",
                Surname = "X_Surname1",
                Address = new Address("123 X_Street1 111")
            };

            var s_p = new Person
            {
                Name = "S_P1",
                Surname = "S_Surname1",
                Address = new Address("123 S_Street1 111")
            };

            var s_p2 = new Person
            {
                Name = "S_P2",
                Surname = "S_Surname1",
                Address = new Address("123 S_Street2 111")
            };

            var z_p2 = new Person
            {
                Name = "Z_P2",
                Surname = "Z_Surname",
                Address = new Address("123 Z_Street1 1111")
            };

            //Act
            people.Add(z_p1);
            people.Add(x_p);
            people.Add(s_p);
            people.Add(s_p2);
            people.Add(z_p2);

            var sortedFrequency = people.Task1SortOrderWithFrequency();

            //Asset
            Assert.AreEqual(sortedFrequency[0].Key, "S_Surname1");
            Assert.AreEqual(sortedFrequency[0].Value, 2);

            Assert.AreEqual(sortedFrequency[1].Key, "Z_Surname");
            Assert.AreEqual(sortedFrequency[1].Value, 2);

            Assert.AreEqual(sortedFrequency[sortedFrequency.Capacity - 1].Key, "Z_P2");
            Assert.AreEqual(sortedFrequency[sortedFrequency.Capacity - 1].Value, 1);

        }

        [TestMethod()]
        public void Given_KeyAnUorderedCollection_When_Task1SortOrderWithFrequencyTest_Then_OrderBy_Desc_Then_Asc_Generics()
        {
            List<Person> people = new List<Person>();

            var z_p1 = new Person
            {
                Name = "Z_P",
                Surname = "Z_Surname",
                Address = new Address("123 Z_Street1 111")
            };

            var x_p = new Person
            {
                Name = "X_P",
                Surname = "X_Surname1",
                Address = new Address("123 X_Street1 111")
            };

            var s_p = new Person
            {
                Name = "S_P1",
                Surname = "S_Surname1",
                Address = new Address("123 S_Street1 111")
            };

            var s_p2 = new Person
            {
                Name = "S_P2",
                Surname = "S_Surname1",
                Address = new Address("123 S_Street2 111")
            };

            var z_p2 = new Person
            {
                Name = "Z_P2",
                Surname = "Z_Surname",
                Address = new Address("123 Z_Street1 1111")
            };

            //Act
            people.Add(z_p1);
            people.Add(x_p);
            people.Add(s_p);
            people.Add(s_p2);
            people.Add(z_p2);

            var sortedFrequency = people.SortFrequencyOrder();
            
            //Asset
            Assert.AreEqual(sortedFrequency[0].Key, "S_Surname1");
            Assert.AreEqual(sortedFrequency[0].Count, 2);

            Assert.AreEqual(sortedFrequency[1].Key, "Z_Surname");
            Assert.AreEqual(sortedFrequency[1].Count, 2);

            Assert.AreEqual(sortedFrequency[sortedFrequency.Capacity - 1].Key, "Z_P2");
            Assert.AreEqual(sortedFrequency[sortedFrequency.Capacity - 1].Count, 1);
        }
    }
}