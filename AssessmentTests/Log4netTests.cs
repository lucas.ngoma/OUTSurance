﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using log4net;
using log4net.Config;
using System.Reflection;

namespace AssessmentTests
{
    /// <summary>
    /// Represents the log4net tests class.
    /// </summary>
    [TestClass]
    public class Log4netTests
    {
        /// <summary>
        /// Log4nets the writes to disk.
        /// </summary>
        [TestMethod]
        [TestCategory("Unit")]
        public void Log4net_WritesToDisk()
        {
            var log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

            var logFile = $"{Path.Combine(Directory.GetCurrentDirectory())}\\logfile.log";

            var span = DateTime.Now.TimeOfDay;

            var messageAt = $"{span.Minutes}:{ span.Seconds}:{ span.Milliseconds}";

            GlobalContext.Properties["LogFileName"] = logFile;

            XmlConfigurator.Configure();

            log.Info($"Message from test @{messageAt}");

            LogManager.Shutdown();

            Assert.IsTrue(File.ReadAllText(logFile).Contains(($"Message from test @{messageAt}")));
        }
    }
}
